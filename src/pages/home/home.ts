import { Component } from '@angular/core';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	constructor() {
		this.initializeItems();
	}

	initializeItems() {
		this.items = [
		{
			titulo:"FAMAS",
			img: "http://ff.garena.com/statics/ff/images/weapon/famas.png"
		},
		{
			titulo:"M500",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M500.png"
		},
		{
			titulo:"MP40",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/MP40.png"
		},
		{
			titulo:"M79",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M79.png"
		},
		{
			titulo:"KAR98k",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/Kar98k.png"
		},
		{
			titulo:"M1873",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M1873.png"
		},
		{
			titulo:"M249",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M249.png"
		},
		{
			titulo:"GRANADA",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/Grenade.png"
		},
		{
			titulo:"M4A1",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M4A1.png"
		},
		{
			titulo:"AK",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/AK.png"
		},
		{
			titulo:"AWM",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/AWM.png"
		},
		{
			titulo:"SKS",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/SKS.png"
		},
		{
			titulo:"GROZA",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/GROZA.png"
		},
		{
			titulo:"M1014",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M1014.png"
		},
		{
			titulo:"UMP",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/UMPSG.png"
		},
		{
			titulo:"MP5",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/MP5.png"
		},
		{
			titulo:"M14",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/M14.png"
		},
		{
			titulo:"SCAR",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/SCAR.png"
		},
		{
			titulo:"VSS",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/VSS.png"
		},
		{
			titulo:"USP",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/USP.png"
		},
		{
			titulo:"G18",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/G18.png"
		},
		{
			titulo:"DESERT EAGLE7",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/EAGLE.png"
		},
		{
			titulo:"SARTEN",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/Pan.png"
		},
		{
			titulo:"MACHETE",
			img: "http://freefiremobile-a.akamaihd.net/ffwebsite/images/weapon/Parang.png"
		}
		];
	}

	getItems(searchbar) {
		console.log(searchbar.target.value);
  // Reset items back to all of the items
  this.initializeItems();
  // set q to the value of the searchbar
  var q = searchbar.target.value;
  // if the value is an empty string don't filter the items
  if (q.trim() == '') {
  	return;
  }

  this.items = this.items.filter((v) => {

  	if (v.titulo.toLowerCase().indexOf(q.toLowerCase()) > -1) {
  		return true;
  	}

  	return false;
  })

}
}